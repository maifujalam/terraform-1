from google.cloud import storage
import os


def create_bucket():
    bucket_name = "bucket-alam-api"
    storage_client=storage.Client()
    bucket=storage_client.create_bucket(bucket_or_name=bucket_name)
    print("Bucket {} created".format(bucket.name))


if __name__ == '__main__':
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = '/home/skmaifuj/Desktop/Projects/Terraform/GCP/service-account/owner1.json'
    create_bucket()
