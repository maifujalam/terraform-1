from google.cloud import storage
import os


def delete_bucket():
    storage_client = storage.Client()
    bucket_name = 'sb-pch3-2'
    bucket = storage_client.get_bucket(bucket_or_name=bucket_name)
    bucket.delete()
    print("Bucket {} Deleted".format(bucket.name))


if __name__ == '__main__':
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = '/home/gslab/Desktop/SoL1/GCP/ServiceAccounts/full.json'
    delete_bucket()
