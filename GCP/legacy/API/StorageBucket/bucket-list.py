from google.cloud import storage
import os


def list_buckets():
    storage_client = storage.Client()
    buckets = storage_client.list_buckets()
    for bucket in buckets:
        print(bucket.name)


if __name__ == '__main__':
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = '/home/gslab/Desktop/SoL1/GCP/ServiceAccounts/full.json'
    list_buckets()
