import googleapiclient.discovery
import os
import json


def create_api_object():  # GCP python client library based on http
    # credentials = GoogleCredentials.get_application_default()
    # service = discovery.build('compute', 'v1', credentials=credentials)
    compute = googleapiclient.discovery.build('compute', version='v1')  # Build and initialize the
    # 'compute' api object(i.e service).
    return compute


def create_vms():
    compute_object = create_api_object()
    instance_body = {
        "kind": "compute#instance",
        "name": instanceName,
        "zone": "projects/" + project + "/zones/" + zone,
        "machineType": "projects/" + project + "/zones/" + zone + "/machineTypes/" + machineType,
        "disks": [
            {
                "kind": "compute#attachedDisk",
                "type": "PERSISTENT",
                "boot": 'true',
                "mode": "READ_WRITE",
                "autoDelete": 'true',
                "deviceName": "instance-2",
                "initializeParams": {
                    "sourceImage": "projects/ubuntu-os-cloud/global/images/" + diskImage,
                    "diskType": "projects/kubernetes1-275318/zones/" + zone + "/diskTypes/pd-standard",
                    "diskSizeGb": "10"
                },
            }
        ],
        "networkInterfaces": [
            {
                "kind": "compute#networkInterface",
                "subnetwork": "projects/" + project + "/regions/us-central1/subnetworks/default",
                "accessConfigs": [
                    {
                        "kind": "compute#accessConfig",
                        "name": "External NAT",
                        "type": "ONE_TO_ONE_NAT",
                        "networkTier": "PREMIUM"
                    }
                ],
            }
        ],
    }
    request = compute_object.instances().insert(project=project, zone=zone, body=instance_body)
    repose = request.execute()
    print(json.dumps(repose, indent=2))


if __name__ == '__main__':
    project = 'kubernetes1-275318'
    zone = 'us-central1-b'
    instanceName = "instance-2"
    machineType = 'n1-standard-1'
    diskImage = 'ubuntu-minimal-1804-bionic-v20200610'
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = '/home/gslab/Desktop/SoL1/GCP/ServiceAccounts/full.json'
    print(create_vms())
    print("You can ssh to machine with below command")
    print("gcloud beta compute ssh --zone "+zone+" --project "+project+" "+instanceName)
