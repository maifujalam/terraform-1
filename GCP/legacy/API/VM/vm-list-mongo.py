import googleapiclient.discovery as gd
import os,pymongo
import json
from oauth2client.client import GoogleCredentials


def create_api_object():  # GCP python client library based on http
    compute = gd.build('compute', version='v1')  # Build and initialize the 'compute' api object(i.e service).
    return compute


def list_vms():
    compute_object = create_api_object()
    project = 'kubernetes1-275318'
    zone = 'us-central1-b'
    result = compute_object.instances().list(project=project, zone=zone).execute()
    print(json.dumps(result, indent=2))
    return result['items'] if 'items' in result else None


if __name__ == '__main__':
    mongo_client=pymongo.MongoClient()
    doc=mongo_client['SOL']['t1'].find({})
    v=doc[0].get('a')
    with open("/home/gslab/Desktop/SoL1/GCP/repo-tmp/sa-admin.json", "w+") as outfile:
        outfile.write(json.dumps(v,indent=4))
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = '/home/gslab/Desktop/SoL1/GCP/repo-tmp/sa-admin.json'
    # print("ENV: ",os.environ.get('GOOGLE_APPLICATION_CREDENTIALS'))
    # aa=json.dumps(a)
    # print(type(aa))
    # credentials = GoogleCredentials.from_json()
    # print(credentials)
    # print(type(credentials))
    # print("Type A",type(a))
    print(list_vms())
