import googleapiclient.discovery
import os
import json


def create_api_object():  # GCP python client library based on http
    compute = googleapiclient.discovery.build('compute', version='v1')  # Build and initialize the 'compute'
    # api object(i.e service).
    return compute


def stop_vms():
    compute_object = create_api_object()
    project = 'kubernetes1-275318'
    zone = 'us-central1-a'
    instance = 'instance-1'
    request = compute_object.instances().start(project=project, zone=zone, instance=instance)
    repose = request.execute()
    print(json.dumps(repose, indent=2))


if __name__ == '__main__':
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = '/home/gslab/Desktop/SoL1/GCP/ServiceAccounts/full.json'
    print(stop_vms())
