# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/google" {
  version     = "3.36.0"
  constraints = ">= 3.36.0"
  hashes = [
    "h1:1hh3gFlkHRAlB2pzxUyBP9mmG7YlFUNbvs0wpH2OkuA=",
  ]
}
