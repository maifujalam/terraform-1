1. terraform init
2. terraform plan -var-file=values.tfvars -var-file="secrets.tfvars"
3. terraform apply -var-file=values.tfvars -var-file="secrets.tfvars" -auto-approve
4. terraform output > bucket
5. terraform destroy -var-file="values.tfvars" -var-file="secrets.tfvars"
6. terraform refresh -var-file=values.tfvars -var-file="secrets.tfvars" (need to refresh after adding output post instance creation)
7. terraform show   
