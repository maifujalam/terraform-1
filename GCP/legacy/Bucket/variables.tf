variable "credential_file" {
  default = "/home/skmaifuj/Desktop/Projects/Terraform/GCP/service-account/admin-sa.json"
}
variable "region" {
  default = "us-central1"
}
variable "zone" {
  default = "us-central1-a"
}
variable "project" {
  default = "k8s-1-286619"
}
variable "bucket_name" {
  default = "bucket-default"
}
variable "force_destroy" {
  description = "true-> force destroy without bothering about bucket content. false-> does not delete bucket if have objects."
  default = true
}
variable "uniform_access" {
  description = "when set to true-> All bucket object have same access, false-> Granular object access"
  default = true
}
variable "location" {
  description = "The location of the bucket to create"
  default = "ASIA-SOUTH1"
}
variable "encryption_key" {
  default = ""
}
