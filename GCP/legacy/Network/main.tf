terraform {
  required_version = ">=0.12.6"
  required_providers {
    google = ">=3.36.0"
  }
  backend "gcs" {
    bucket      = "bucket-1_k8s-1-286619"
    prefix      = "/terraform/state/network/network-1"
    credentials = "../service-account/owner1.json"
  }
}
provider "google" {
  credentials = file(var.credential_file)
  project     = var.project
  region      = var.region
  zone        = var.zone
}
resource "google_compute_network" "vpc_network" {
  name = var.network
  description = "Network 1"
}
