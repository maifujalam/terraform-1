terraform apply -var-file=values.tfvars
terraform output > vm
ssh-keygen -f keys/vm
ssh -i keys/vm skmaifuj@34.123.87.15
terraform destroy


note: Make sure in terraform directory
0. cd master1
1. terraform init
2. ssh-keygen -f keys/vm
3. sudo chmod u=rw,g=,o= keys/{vm,vm1}
4. terraform plan -var-file=values.tfvars -var-file="secrets.tfvars"
5. terraform apply -var-file=values.tfvars -var-file="secrets.tfvars" -auto-approve
6. terraform output > ansible-tower
7. terraform destroy -var-file="values.tfvars" -var-file="secrets.tfvars"
8. terraform refresh -var-file=values.tfvars -var-file="secrets.tfvars" (need to refresh after adding output post instance creation)
9. terraform show

Notes:
0. /etc/rhsm/ca/
1. sudo su 
2. mkdir /ansible-tower && cd /ansible-tower 
3. sudo yum -y install ansible vim curl
4. curl -k -O https://releases.ansible.com/ansible-tower/setup/ansible-tower-setup-latest.tar.gz
5. tar xvf ansible-tower-setup-latest.tar.gz
6. cd ansible-tower-setup-*
7. edit inventory -> admin-pass: root,pg-password:root
8. ./setup.sh




