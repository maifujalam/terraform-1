terraform {
  required_version = ">=0.12.6"
  required_providers {
    google = ">=3.36.0"
  }
  backend "gcs" {
    bucket      = "bucket-1_k8s-1-286619"
    prefix      = "/terraform/state/AnsibleTower"
    credentials = "../../service-account/owner1.json"
  }
}
provider "google" {
  credentials = file(var.credential_file)
  project     = var.project
  region      = var.region
  zone        = var.zone
}
resource "google_compute_address" "static_ip" {
  name = "static-ip"
}
resource "google_compute_firewall" "firewall-1" {
  name          = "${var.vm_network}-fireall"
  network       = var.vm_network
  allow {
    protocol = "icmp"
  }
  allow {
    protocol = "tcp"
    ports    = [
      "22",
      "80",
      "8080",
      "443"]
  }
  source_ranges = [
    "0.0.0.0/0"]
}
resource "google_compute_instance" "vm" {
  machine_type              = var.machine_types[var.environment]
  name                      = var.instance_name
  enable_display            = true
  allow_stopping_for_update = true
  tags                      = [
    var.environment,
    var.project,
    var.instance_name]
  boot_disk {
    initialize_params {
      image = var.boot_images.centos-7
      size  = var.boot_disk_size
      type  = var.boot_disk_type.hdd
    }
  }
  network_interface {
    network = var.vm_network
    access_config {
      nat_ip = google_compute_address.static_ip.address
    }
  }
  metadata                  = {
    ssh-keys = "${var.gce_ssh_user}:${file(var.gce_ssh_pub_key_file)}"
  }
}

