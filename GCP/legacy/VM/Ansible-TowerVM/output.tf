output "instance_name" {
  value = google_compute_instance.vm.name
}
output "instance_ip" {
  value = google_compute_instance.vm.network_interface[0].access_config[0].nat_ip
}
output "machine_type" {
  value = google_compute_instance.vm.machine_type
}
output "internal_ip" {
  value = google_compute_instance.vm.network_interface[0].network_ip
}
output "disk_type" {
  value = google_compute_instance.vm.boot_disk[0].initialize_params[0].type
}
output "ssh_vm" {
  value = "ssh -i keys/vm ${var.gce_ssh_user}@${google_compute_address.static_ip.address}"
}
