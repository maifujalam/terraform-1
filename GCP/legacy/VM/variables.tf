variable "credential_file" {
  default = "/home/skmaifuj/Desktop/Projects/Terraform/GCP/service-account/admin-sa.json"
}
variable "region" {
  default = "us-central1"
}
variable "zone" {
  default = "us-central1-a"
}
variable "project" {
  default = "k8s-1-286619"
}
variable "environment" {
  type    = string
  default = "dev"
}
variable "gce_ssh_user" {
  default = "skmaifuj"
}
variable "gce_ssh_pub_key_file" {
  default = "/home/skmaifuj/Desktop/Projects/Terraform/GCP/VM/keys/vm.pub"
}
variable "instance_name" {
  default = "VM-Default-1"
}
variable "machine_types" {
  type    = map(string)
  default = {
    dev  = "f1-micro"
    test = "n1-standard-1"
    prod = "n1-highcpu-4"
  }
}
variable "machine_tags" {
  type = list(string)
  default = ["web", "dev"]
}
variable "boot_images" {
  type    = map(string)
  default = {
    "ubuntu-18.04" = "ubuntu-1804-bionic-v20200821a",
    "ubuntu-20.04" = "ubuntu-2004-focal-v20200810",
    "ubuntu-20.04-minimal" = "ubuntu-minimal-2004-focal-v20200826",
    centos-7 = "centos-7-v20200811",
    centos-8 = "centos-8-v20200811",
    rhel-7 = "rhel-7-v20200811",
    rhel-8 = "rhel-8-v20200811",
    cos-81 = "cos-81-12871-1190-0"
  }
}
variable "boot_disk_size" {
  default = "20"
}
variable "boot_disk_type" {
  type = map(string)
  default = {
    hdd = "pd-standard",
    ssd = "pd-ssd"
  }
}
variable "vm_network" {
  default = "network-1"
}
variable "bucket_name" {
  type = string
  default = "bucket_alam1"
}

variable "bucket_location" {
  type = string
  default = "US"
}
