terraform {
  required_version = ">=0.12.6"
  required_providers {
    google = ">=3.35.0"
  }
}
provider "google" {
  credentials = file(var.credential_file)
  project = var.project
  region = var.region
  zone = var.zone
}

resource "google_storage_bucket" "bucket" {
  name = format("%s_%s",var.bucket_name,var.project)
  force_destroy = var.force_destroy
  bucket_policy_only = var.uniform_access
  location = var.location
}
