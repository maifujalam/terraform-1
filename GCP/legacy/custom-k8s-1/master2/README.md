note: Make sure in terraform directory
0. cd master1
1. terraform init
3. sudo chmod u=rw,g=,o= keys/vm
4. terraform plan -var-file=values.tfvars -var-file="secrets.tfvars"
5. terraform apply -var-file=values.tfvars -var-file="secrets.tfvars"
6. terraform output > vm
7. terraform destroy -var-file="values.tfvars" -var-file="secrets.tfvars"
