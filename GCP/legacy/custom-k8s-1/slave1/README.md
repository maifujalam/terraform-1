note: Make sure in terraform directory
1. cd template
2. update vm_name,bucket prefix name,
3. terraform init
4. sudo chmod u=rw,g=,o= keys/vm
5. terraform plan -var-file=values.tfvars -var-file="secrets.tfvars"
6. terraform apply -var-file=values.tfvars -var-file="secrets.tfvars"
7. terraform output > vm
8. terraform destroy -var-file="values.tfvars" -var-file="secrets.tfvars"
