region = "asia-east1"
zone = "asia-east1-a"
project = "k8s-1-286619"
instance_group_name = "c1-worker"
vm_count = 2
environment = "test"
boot_disk_size = 20
gce_ssh_pub_key_file = "keys/vm.pub"
