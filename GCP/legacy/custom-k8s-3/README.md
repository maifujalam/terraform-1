note: Make sure in terraform directory
0. cd master1
1. terraform init
2. ssh-keygen -f keys/vm
3. sudo chmod u=rw,g=,o= keys/*
4. terraform plan -var-file=values.tfvars -var-file="secrets.tfvars"
5. terraform apply -var-file=values.tfvars -var-file="secrets.tfvars" -auto-approve
6. terraform output > master
7. terraform destroy -var-file="values.tfvars" -var-file="secrets.tfvars" -auto-approve
8. terraform refresh -var-file=values.tfvars -var-file="secrets.tfvars" (need to refresh after adding output post instance creation)
9. terraform show
