terraform {
  required_version = ">=0.12.6"
  required_providers {
    google = ">=3.36.0"
  }
  backend "gcs" {
    bucket      = "bucket-1_k8s-1-286619"
    prefix      = "terraform/state/g2"
    # Variables not allowed hare
    credentials = "../service-account/owner1.json"
  }
}
provider "google" {
  credentials = file(var.credential_file)
  project     = var.project
  region      = var.region
  zone        = var.zone
}

resource "google_compute_address" "static_ip" {
  count = var.master-count
  name  = "${var.master-name}-${count.index+1}-static-ip"
}
resource "google_compute_firewall" "internal" {
  name          = "internal-firewall"
  network       = var.network
  allow {
    protocol = "icmp"
  }
  allow {
    protocol = "udp"
  }
  allow {
    protocol = "tcp"
  }
  source_ranges = [
    var.subnet-cidr]
}
resource "google_compute_firewall" "external" {
  name          = "external-firewall"
  network       = var.network
  allow {
    protocol = "icmp"
  }
  allow {
    protocol = "udp"
  }
  allow {
    protocol = "tcp"
    ports    = [
      "22",
      "6443",
      "30000-32767"]
  }
  source_ranges = [
    "0.0.0.0/0"]
}
resource "google_compute_instance" "master" {
  count                     = var.master-count
  name                      = "${var.master-name}-${count.index+1}"
  machine_type              = var.machine_types[var.environment]
  allow_stopping_for_update = true
  can_ip_forward            = true
  boot_disk {
    initialize_params {
      image = var.boot_images[var.os-name]
      size  = var.boot_disk_size
      type  = var.boot_disk_type["ssd"]
    }
  }
  network_interface {
    network = var.network
    access_config {
      nat_ip = element(google_compute_address.static_ip.*.address, count.index )
    }
  }
  metadata                  = {
    ssh-keys = "${var.gce_ssh_user}:${file(var.gce_ssh_pub_key_file)}"
  }
  //  metadata_startup_script = "sudo yum update -y"
}
resource "google_compute_instance" "worker" {
  count                     = var.worker-count
  name                      = "${var.worker-name}-${count.index+1}"
  machine_type              = var.machine_types[var.environment]
  allow_stopping_for_update = true
  can_ip_forward            = true
  boot_disk {
    initialize_params {
      image = var.boot_images[var.os-name]
      size  = var.boot_disk_size
      type  = var.boot_disk_type.ssd
    }
  }
  network_interface {
    network = var.network
    access_config {
    }
  }
  metadata                  = {
    ssh-keys = "${var.gce_ssh_user}:${file(var.gce_ssh_pub_key_file)}"
  }
  //  metadata_startup_script = "sudo yum update -y"
}
