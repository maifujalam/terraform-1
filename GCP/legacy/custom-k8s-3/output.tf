output "master_name" {
  value = google_compute_instance.master[*].name
}
output "master_ip" {
  value = google_compute_instance.master[*].network_interface[0].access_config[0].nat_ip
}
output "machine_type" {
  value = google_compute_instance.master.*.machine_type
}
output "internal_ip" {
  value = google_compute_instance.master[*].network_interface[0].network_ip
}
output "disk_type" {
  value = google_compute_instance.master[*].boot_disk[0].initialize_params[0].type
}
output "ssh_vm" {
  value = formatlist("%s  =  ssh -i keys/vm %s@%s",google_compute_instance.master[*].name,var.gce_ssh_user,google_compute_address.static_ip[*].address)
}
