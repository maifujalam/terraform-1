region  = "us-central1"
zone    = "us-central1-a"
project = "k8s-1-286619"

master-name    = "c1-master"
worker-name    = "c1-worker"
master-count   = 1
worker-count   = 2
environment    = "test"
boot_disk_size = 20
os-name = "rhel-7"


network     = "network-1"
subnet-name = "subnet-1"
subnet-cidr = "10.128.0.0/24"

gce_ssh_pub_key_file = "keys/vm.pub"
