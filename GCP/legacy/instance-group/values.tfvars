region              = "us-central1"
zone                = "us-central1-a"
project             = "k8s-1-286619"
instance_group_name = "c1-master"
master_count        = 1
environment         = "test"
boot_disk_size      = 20
