variable "credential_file" {
  default = "/home/skmaifuj/Desktop/Projects/Terraform/GCP/service-account/admin-sa.json"
}
variable "region" {
  default = "us-central1"
}
variable "zone" {
  default = "us-central1-a"
}
variable "project" {
  default = "k8s-1-286619"
}
variable "instance_group_name" {
  default = "default-instance-group"
}
variable "master_count" {
  default = 3
}
variable "environment" {
  type    = string
  default = "dev"
}
variable "machine_tags" {
  type    = list(string)
  default = [
    "web",
    "dev"]
}
variable "machine_types" {
  type    = map(string)
  default = {
    dev   = "f1-micro"
    # cpu-1,mem-0.6
    test  = "e2-small"
    # cpu-2,mem-2
    prod  = "n1-highcpu-4"
    # cpu-4,mem-3.6
    test2 = "g1-small"
    # cpu-1,mem-1.7
  }
}
variable "boot_disk_size" {
  default = "15"
}
variable "boot_disk_type" {
  type    = map(string)
  default = {
    hdd = "pd-standard",
    ssd = "pd-ssd"
  }
}
variable "boot_images" {
  type    = map(string)
  default = {
    "ubuntu-18.04"         = "ubuntu-1804-bionic-v20201116",
    "ubuntu-20.04"         = "ubuntu-2004-focal-v20201111",
    "ubuntu-20.04-minimal" = "ubuntu-minimal-2004-focal-v20201111",
    centos-7               = "centos-7-v20201112",
    centos-8               = "centos-8-v20201112",
    rhel-7                 = "rhel-7-v20201112",
    rhel-8                 = "rhel-8-v20201112",
    cos-81                 = "cos-81-12871-1190-0"
  }
}
variable "vm_network" {
  default = "default"
}
variable "gce_ssh_user" {
  default = "skmaifuj"
}
variable "gce_ssh_pub_key_file" {
  default = "keys/vm.pub"
}
