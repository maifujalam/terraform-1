terraform {
  required_version = ">=0.12.6"
  required_providers {
    google = ">=3.36.0"
  }
  backend "gcs" {
    bucket = "bucket-1_k8s-1-286619"
    prefix  = "terraform/state/g1/worker"   # Variables not allowed hare
    credentials = "../../service-account/owner1.json"
  }
}
provider "google" {
  credentials = file(var.credential_file)
  project = var.project
  region = var.region
  zone = var.zone
}
resource "google_compute_address" "static_ip" {
  count = var.master_count
  name = "${var.instance_group_name}-${count.index+1}-static-ip"
}
resource "google_compute_instance" "vm" {
  count = var.master_count
  name = "${var.instance_group_name}-${count.index+1}"
  machine_type = var.machine_types[var.environment]
  boot_disk {
    initialize_params {
    image = var.boot_images["rhel-7"]
    size = var.boot_disk_size
    type = var.boot_disk_type["ssd"]
  }
  }
  network_interface {
    network = var.vm_network
    access_config {
      nat_ip = element(google_compute_address.static_ip.*.address,count.index )
    }
  }
  metadata = {
    ssh-keys="${var.gce_ssh_user}:${file(var.gce_ssh_pub_key_file)}"
  }
}

