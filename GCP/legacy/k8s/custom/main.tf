terraform {
  required_version = ">=0.12.6"
  required_providers {
    google = ">=3.58.0"
  }
  backend "gcs" {
    bucket = "bucket-1_k8s-1-286619"
    prefix  = "terraform/state/Clusters_k8s/my-gke-cluster"   # Variables not allowed hare
    credentials = "../../service-account/owner1.json"
  }
}
provider "google" {
  credentials = file(var.credential_file)
  project = var.project
  region = var.region
  zone = var.zone
}
data "google_container_engine_versions" "k8s-version" {
  location       = var.zone
  version_prefix = var.k8s-version
}
resource "google_container_cluster" "primary" {
  name     = var.cluster_name
  location = var.zone
  remove_default_node_pool = true
  initial_node_count       = 1
  min_master_version = data.google_container_engine_versions.k8s-version.release_channel_default_version["REGULAR"]
  node_version = data.google_container_engine_versions.k8s-version.release_channel_default_version["REGULAR"]

}
resource "google_container_node_pool" "primary_preemptible_nodes" {
  name       = var.cluster_name
  location   = var.zone
  cluster    = google_container_cluster.primary.name
  node_count = 1
  node_config {
    machine_type = "n1-standard-1"
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
  autoscaling {
    max_node_count = 3
    min_node_count = 1
  }
  timeouts {
    create = "30m"
    update = "20m"
  }
//  version = data.google_container_engine_versions.k8s-version.latest_node_version
}
module "gke_auth" {
  depends_on = [google_container_cluster.primary]
  source           = "terraform-google-modules/kubernetes-engine/google//modules/auth"
  project_id       = var.project
  cluster_name     = var.cluster_name
  location         = var.zone
}
resource "local_file" "kubeconfig" {
  depends_on = [module.gke_auth]
  content  = module.gke_auth.kubeconfig_raw
  filename = "${path.module}/config"
  file_permission = "0644"
}
