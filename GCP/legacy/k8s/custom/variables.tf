variable "credential_file" {
  default = "/home/skmaifuj/Desktop/Projects/Terraform/GCP/service-account/admin-sa.json"
}
variable "region" {
  default = "asia-east1"
}
variable "zone" {
  default = "asia-east1-a"
}
variable "project" {
  default = "k8s-1-286619"
}
variable "cluster_name" {
  default = "cluster-default"
}
variable "k8s-version" {
  default = ""
}
variable "min-k8s-version" {
  default = ""
}
