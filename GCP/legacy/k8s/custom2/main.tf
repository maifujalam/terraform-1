terraform {
  required_version = ">=0.12.6"
  required_providers {
    google = ">=3.36.0"
  }
}
provider "google" {
  credentials = file(var.credential_file)
  project = var.project
  region = var.region
  zone = var.zone
}
data "google_container_engine_versions" "k8s-version" {
  location       = var.zone
  version_prefix = var.k8s-version
}
resource "google_container_cluster" "primary" {
  name     = var.cluster_name
  location = var.zone
  remove_default_node_pool = true
  initial_node_count       = 1
  node_version = data.google_container_engine_versions.k8s-version.latest_node_version
  min_master_version = data.google_container_engine_versions.k8s-version.latest_node_version
  cluster_ipv4_cidr = var.pod_cidr
  addons_config {
    horizontal_pod_autoscaling {
      disabled = false
    }
    http_load_balancing {
      disabled = false
    }
  }
  master_auth {
    client_certificate_config {
      issue_client_certificate = true
    }
  }
}
resource "google_container_node_pool" "primary_preemptible_nodes" {
  name       = var.cluster_name
  location   = var.zone
  cluster    = google_container_cluster.primary.name
  node_count = var.node_count

  node_config {
    machine_type = var.machine_type
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
    disk_type = var.boot_disk_type
    disk_size_gb = var.boot_disk_size
    image_type = var.image_type
  }
  autoscaling {
    max_node_count = var.max_node_count
    min_node_count = 1
  }
  timeouts {
    create = "30m"
    update = "20m"
  }
}
module "gke_auth" {
  depends_on = [google_container_cluster.primary]
  source           = "terraform-google-modules/kubernetes-engine/google//modules/auth"
  project_id       = var.project
  cluster_name     = var.cluster_name
  location         = var.zone
}
resource "local_file" "kubeconfig" {
  depends_on = [module.gke_auth]
  content  = module.gke_auth.kubeconfig_raw
  filename = "${path.module}/config"
}
module "kubernetes-engine_auth" {
  source  = "terraform-google-modules/kubernetes-engine/google//modules/auth"
//  version = "11.1.0"
  # insert the 3 required variables here
  cluster_name = var.cluster_name
  location = var.zone
  project_id = var.project
}
resource "local_file" "kubeconfig2" {
  depends_on = [module.gke_auth]
  content  = module.kubernetes-engine_auth.kubeconfig_raw
  filename = "${path.module}/config2"
}
