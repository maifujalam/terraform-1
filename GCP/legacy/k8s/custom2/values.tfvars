region = "asia-east1"
zone = "asia-east1-a"
project = "k8s-1-286619"

cluster_name = "cluster-1"
image_type = "ubuntu"     // it can be COS
k8s-version = "1.17."  //Add dot after the minor version
machine_type = "g1-small"  //Machine type can  be: n1-standard-1
node_count = 1
max_node_count = 3
pod_cidr = "192.168.0.0/16"
boot_disk_type = "pd-ssd"
boot_disk_size = 20


