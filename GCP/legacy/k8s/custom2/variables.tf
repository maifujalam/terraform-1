variable "credential_file" {
  default = "/home/skmaifuj/Desktop/Projects/Terraform/GCP/service-account/admin-sa.json"
}
variable "region" {
  default = "asia-east1"
}
variable "zone" {
  default = "asia-east1-a"
}
variable "project" {
  default = "k8s-1-286619"
}
variable "cluster_name" {
  default = "cluster-default"
}
variable "k8s-version" {
  default = "1.17."
}
variable "k8s-ver" {
  default = "1.17.9-gke.1504"
}
variable "machine_type" {
  default = "n1-standard-1"
}
variable "image_type" {
  default = "COS"
}
variable "node_count" {
  default = 1
}
variable "max_node_count" {
  default = 10
}
variable "pod_cidr" {
  default = "192.168.0.0/16"
}
variable "boot_disk_type" {
  default = "pd-standard"
}
variable "boot_disk_size" {
  default = 15
}
