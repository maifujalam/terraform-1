terraform {
  required_version = ">=0.12.6"
  required_providers {
    google = ">=3.36.0"
  }
}
provider "google" {
  credentials = file(var.credential_file)
  project = var.project
  region = var.region
  zone = var.zone
}
resource "google_container_cluster" "cluster" {
  name = var.cluster_name
  location = var.zone
  initial_node_count = 3
}
module "gke_auth" {
  source           = "terraform-google-modules/kubernetes-engine/google//modules/auth"
  project_id       = var.project
  cluster_name     = var.cluster_name
  location         = var.zone
}
resource "local_file" "kubeconfig" {
  content  = module.gke_auth.kubeconfig_raw
  filename = "${path.module}/kubeconfig"
}
