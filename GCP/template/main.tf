terraform {
  required_version = ">=0.12.6"
  required_providers {
    google = ">=3.36.0"
  }
}
provider "google" {
  credentials = file(var.credential_file)
  project = var.project
  region = var.region
  zone = var.zone
}
