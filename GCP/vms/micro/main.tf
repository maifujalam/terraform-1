terraform {
  required_version = ">=1.0.3"
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">=3.77.0"
    }
  }
}
provider "google" {
  credentials = file(var.credential_file)
  project     = var.project
  region      = var.region
  zone        = var.zone
}
data "google_compute_network" "default-network" {
  name = "default"
}
resource "google_compute_instance" "vm" {
  machine_type = var.machine_type
  name         = "sample-vm"
  tags = var.vm_tags
  boot_disk {
    initialize_params {
      image = var.image.centos8
      size  = var.boot_disk_size
      type  = var.boot_disk_type.hdd
    }
  }
  network_interface {
    network = data.google_compute_network.default-network.name
    access_config {
    }
  }
  metadata     = {
    ssh-keys = "${var.ssh_user}:${file(var.ssh_key)}"
  }
}

