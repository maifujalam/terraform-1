variable "credential_file" {
  default = "/home/skmaifuj/Desktop/Projects/Terraform/GCP/service-accounts/sa1.json"
}
variable "project" {
  default = "sample-project-321605"
}
variable "region" {
  default = "us-central1"
}
variable "zone" {
  default = "us-central1-a"
}
variable "machine_type" {
  default = "f1-micro"
}
variable "image" {
  type    = object({
    centos7 = string
    centos8 = string
  })
  default = {
    centos7 = "centos-7-v20210721"
    centos8 = "centos-8-v20210721"
  }
}
variable "boot_disk_type" {
  type    = map(string)
  default = {
    hdd = "pd-standard"
    ssd = "pd-ssd"
  }
}
variable "boot_disk_size" {
  default = "20"
}
variable "ssh_key" {
  default = ""
}
variable "ssh_user" {
  default = ""
}
variable "vm_tags" {
  type    = list(string)
  default = ["vm","rhel8"]
}
