//terraform {
//  required_version = ">=0.12.6"
//  required_providers {
//    oci = ">=3.87.0"
//  }
//}
provider "oci" {
  tenancy_ocid     = "ocid1.tenancy.oc1..aaaaaaaa53g3ryokspi54d5pwglal233yelbinrcbiq5k6wzusnzncaa3l2q"
  user_ocid        = "ocid1.user.oc1..aaaaaaaagob5ca3qjfltewyntw3i4mjnds42y7t3zmz3daw55auq4fmlgtza"
  fingerprint      = "38:21:be:32:87:79:4b:03:d7:19:1b:b7:61:fd:da:dd"
  private_key_path = "/home/gslab/Desktop/SoL1/OCI/key/oci_api_key_public.pem"
  region           = "ap-mumbai-1"
}
data "oci_identity_availability_domain" "ad" {
  compartment_id = "ocid1.tenancy.oc1..aaaaaaaa53g3ryokspi54d5pwglal233yelbinrcbiq5k6wzusnzncaa3l2q"
  ad_number      = 1
}
data "oci_core_volume_backup_policies" "test_predefine_volume_backup_policy" {
  filter {
    name   = "display_name"
    values = [
      "silver"]
  }
}
//output "silver_policy_id" {
//  value = data.oci_core_volume_backup_policies.test_predefine_volume_backup_policy.volume_backup_policies[0].id
//}
resource "oci_core_volume" "volume1" {
  availability_domain = data.oci_identity_availability_domain.ad.name
  compartment_id      = "ocid1.tenancy.oc1..aaaaaaaa53g3ryokspi54d5pwglal233yelbinrcbiq5k6wzusnzncaa3l2q"
  display_name        = "volume1"
  size_in_gbs         = "55"
}
resource "oci_core_volume_backup_policy_assignment" "volume_policy_assignment" {
  asset_id  = oci_core_volume.volume1.id
  policy_id = data.oci_core_volume_backup_policies.test_predefine_volume_backup_policy.volume_backup_policies[0].id
}
