terraform {
  required_version = ">=0.12.6"
  required_providers {
    oci = ">=3.87.0"
  }
}

provider "oci" {
  tenancy_ocid     = var.tenancy_ocid
  user_ocid        = var.user_ocid
  fingerprint      = var.fingerprint
  private_key_path = var.private_key_path
  region           = var.region
}

data "oci_identity_availability_domain" "ad" {
  compartment_id = var.compartment_id
  ad_number      = 1
}
data "oci_core_volume_backup_policies" "test_predefine_volume_backup_policy" {
  filter {
    name   = "display_name"
    values = [
      "silver"]
  }
}

resource "oci_core_volume" "volume" {
  availability_domain = data.oci_identity_availability_domain.ad.name
  compartment_id      = var.compartment_id
  display_name        = var.display_name
  size_in_gbs         = var.size
}
resource "oci_core_volume_backup_policy_assignment" "volume_policy_assignment" {
  asset_id  = oci_core_volume.volume.id
  policy_id = data.oci_core_volume_backup_policies.test_predefine_volume_backup_policy.volume_backup_policies[0].id
}
