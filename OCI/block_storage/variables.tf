variable "tenancy_ocid" {
  default = ""
}
variable "user_ocid" {
  default = ""
}
variable "fingerprint" {
  default = ""
}
variable "private_key_path" {
  default = ""
}
variable "region" {
  default = "ap-mumbai-1"
}
variable "compartment_id" {
  default = ""
}
variable "display_name" {
  default = "default_display_name"
}
variable "size" {
  default = "50"
}
