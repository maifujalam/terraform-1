#  terraform apply -var-file="secrets.tfvars" -var-file="values.tfvars"
terraform {
  required_version = ">=0.12.6"
  required_providers {
    oci = ">=3.87.0"
  }
}

provider "oci" {
  tenancy_ocid     = var.tenancy_ocid
  user_ocid        = var.user_ocid
  fingerprint      = var.fingerprint
  private_key_path = var.private_key_path
  region           = var.region
}
data "oci_identity_availability_domain" "ad" {
  compartment_id = var.compartment_id
  ad_number      = 1
}
data "oci_objectstorage_namespace" "test_namespace" {
    compartment_id = var.compartment_id
}
resource "oci_objectstorage_bucket" "test_bucket" {
  compartment_id = var.compartment_id
  name           = var.name
  namespace      = data.oci_objectstorage_namespace.test_namespace.namespace
}
