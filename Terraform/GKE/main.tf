terraform {
  required_version = ">=0.12.6"
  required_providers {
    google = ">=3.27.0"
  }
  backend "gcs" {
    bucket = "tf-alam-bucket1"
    prefix = "terraform/state"
  }
}

provider "google" {
  project = var.project
  region  = var.region
  zone    = var.zone
}
resource "google_service_account" "service_account" {
  account_id   = "${var.cluster_name}-sa-1"
  display_name = "${var.cluster_name}-sa-1"
}
resource "google_project_iam_member" "roles" {
  project = var.project
  member = "serviceAccount:${google_service_account.service_account.email}"
  count = length(var.iam_roles)
  //  role   = "roles/owner"
  role   = element(values(var.iam_roles),count.index)
}
