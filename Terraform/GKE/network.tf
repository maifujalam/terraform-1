resource "google_compute_network" "vpc-network" {
  name = "vpc-network"
  auto_create_subnetworks = false
  routing_mode = "REGIONAL"
}

resource "google_compute_subnetwork" "subnet-1" {
  ip_cidr_range = var.subnet_cidr
  name          = var.cluster_name
  network       = google_compute_network.vpc-network.name
}

resource "google_compute_firewall" "firewall-1" {
  name    = "${google_compute_network.vpc-network.name}-fireall"
  network = google_compute_network.vpc-network.name
  source_ranges = [var.subnet_cidr,var.pod_range]
  allow {
    protocol = "icmp"
  }
  allow {
    protocol = "tcp"
    ports = ["80","8080","300000-320000"]
  }
}

resource "google_compute_address" "ip-address" {
  name = ""
}
