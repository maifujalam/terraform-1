project      = "kubernetes1-275318"
zone         = "us-central1-b"
bucket       = "tf-alam-bucket1"
cluster_name = "Cluster-1"
iam_roles    = {
  role1 = "roles/storage.objectViewer"
  role2 = "roles/logging.logWriter"
}
subnet_cidr = "10.10.0.0/16"
pod_range = "192.168.0.0/16"
