variable "project" {
  description = "Project ID"
  default     = "terraform-alam-1"
}
variable "region" {
  default = "us-central1"
}
variable "zone" {
  default = "us-central1-a"
}
variable "bucket" {
  description = "Name of the GCP Bucket"
  default = "tf-alam-bucket1"
}
variable "project_path" {
  description = "Project location in Google Bucket"
  default = "/terraform/default"
}
variable "cluster_name" {
  description = "Name of the Kubernetes Cluster"
  default     = "cluster-1"
}
variable "iam_roles" {
  description = "All roles given to this project"
  type        = map(string)
  default     = {
    role1 = "roles/storage.objectViewer"
    role2 = "roles/logging.logWriter"
  }
}
