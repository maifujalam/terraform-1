terraform {
  required_version = ">=0.12.6"
  required_providers {
    vsphere = ">=1.17.1"
  }
}

provider "vsphere" {
  vsphere_server       = var.server
  user                 = var.user
  password             = var.password
  allow_unverified_ssl = var.allow_unverified_ssl
}

data "vsphere_datacenter" "dc" {
  name = var.dc_name
}

data "vsphere_datastore_cluster" "datastore_cluster" {
  name          = var.datastore_cluster
  count         = var.datastore_cluster != "" ? 0 : 1
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_datastore" "datastore" {
  name          = var.datastore
  datacenter_id = data.vsphere_datacenter.dc.id
  count         = 1
}

data "vsphere_resource_pool" "resource_pool" {
  name          = var.resource_pool
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_network" "network" {
  count         = var.network_cards != null ? length(var.network_cards) : 0
  name          = var.network_cards[count.index]
  datacenter_id = data.vsphere_datacenter.dc.id
}

resource "vsphere_virtual_machines" "cd" {
  name             = ""
  resource_pool_id = ""
}
