variable "server" {
  description = "Server IP"
  type        = string
}
variable "user" {
  description = "Username of VSphere"
  type        = string
}
variable "password" {
  description = "Password of VSphere account"
}
variable "allow_unverified_ssl" {
  description = "Skip SSL Check"
  type        = bool
  default     = true
}
variable "dc_name" {
  description = "Name of Data Center"
}
variable "datastore_cluster" {
  description = "Name of Datastore Cluster"
  default = ""
}
variable "datastore" {
  description = "Name of Datastore (HDD)"
  default = ""
}
variable "resource_pool" {
  description = "Name of the resource pool (CPU & Memory)"
  default = ""
}
variable "network_cards" {
  description = "Names of Network cards"
  default = ""
}
