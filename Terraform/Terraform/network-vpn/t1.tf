provider "google" {
  version = "3.5.0"

  credentials = file("/home/gslab/Desktop/SoL1/GCP/ServiceAccounts/full.json")

  project = "kubernetes1-275318"
  region  = "us-central1"
  zone    = "us-central1-a"
}

resource "google_compute_network" "vpc_network" {
  name = "terraform-network"
}
