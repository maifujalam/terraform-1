provider "google" {
  version     = "3.5.0"
  credentials = file(var.credentials_file)
  project     = var.project
  region      = var.region
  zone        = var.zone

}

resource "google_compute_network" "vpc_network" {
  name = "terraform-network"
}

resource "google_compute_instance" "vm_instance" {
  name           = "terraform-instance"
  machine_type   = var.machine_types[var.environment]
  tags           = var.machine_tags
  enable_display = true
  depends_on     = [google_storage_bucket.bucket_tf]

  boot_disk {
    initialize_params {
      image = var.boot_image
    }
  }
  network_interface {
    network = google_compute_network.vpc_network.name
    access_config {}
  }
}

resource "google_compute_address" "vm_static_ip" {
  name = "terraform-static-ip"
}

resource "google_storage_bucket" "bucket_tf" {
  name     = var.bucket_name
  location = var.bucket_location
  website {
    main_page_suffix = "index.html"
    not_found_page   = "404.html"
  }
}
