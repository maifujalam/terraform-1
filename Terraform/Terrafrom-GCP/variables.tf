variable "project" {
  description = "Specify GCP Project ID."
}

variable "credentials_file" {
  description = "Location of service account json file."
}

variable "region" {
  default = "us-central1"
}

variable "zone" {
  default = "us-central1-a"
}

variable "environment" {
  type    = string
  default = "dev"
}

variable "machine_types" {
  type    = map(string)
  default = {
    dev  = "f1-micro"
    test = "n1-highcpu-32"
    prod = "n1-highcpu-32"
  }
}

variable "machine_tags" {
  type = list(string)
  default = ["web", "dev"]
}

variable "boot_image" {
  type = string
  default = "cos-cloud/cos-stable"
}

variable "bucket_name" {
  type = string
  default = "bucket_alam1"
}

variable "bucket_location" {
  type = string
  default = "US"
}
