from VC import connection, vm_details, edit_vm
from pyVmomi import vim


def add_disk(vm, disk_size, disk_type):
    spec = vim.vm.ConfigSpec()
    devices = []
    unit_number = 0
    new_disk_kb = int(disk_size) * 1024 * 1024
    scsi_ctr = vim.vm.device.VirtualDeviceSpec()
    scsi_ctr.operation = vim.vm.device.VirtualDeviceSpec.Operation.add
    scsi_ctr.device = vim.vm.device.ParaVirtualSCSIController()
    scsi_ctr.device.busNumber = 1
    scsi_ctr.device.hotAddRemove = True
    scsi_ctr.device.sharedBus = 'noSharing'
    scsi_ctr.device.scsiCtlrUnitNumber = 7
    devices.append(scsi_ctr)
    controller = scsi_ctr.device
    disk_spec = vim.vm.device.VirtualDeviceSpec()
    disk_spec.fileOperation = "create"
    disk_spec.operation = vim.vm.device.VirtualDeviceSpec.Operation.add
    disk_spec.device = vim.vm.device.VirtualDisk()
    disk_spec.device.backing = vim.vm.device.VirtualDisk.FlatVer2BackingInfo()
    if disk_type == 'thin':
        disk_spec.device.backing.thinProvisioned = True
    disk_spec.device.backing.diskMode = 'persistent'
    disk_spec.device.unitNumber = unit_number
    disk_spec.device.capacityInKB = new_disk_kb
    disk_spec.device.controllerKey = controller.key
    devices.append(disk_spec)
    spec.deviceChange = devices
    ack=edit_vm.edit_vm(vm_obj=vm,spec=spec)
    print("Ack: ",ack)


def main():
    si = connection.connect_vc(server='10.0.88.202', username='clouduser@vcn.local', password='gsLab!1231')
    if connection is not None:
        print("Session vm: ", si.content.sessionManager.currentSession.key)
    else:
        print("Not Connected to vc!")
        exit(0)
    vm_ip = "10.136.58.157"
    vm = None
    search_index = si.content.searchIndex
    vm = search_index.FindByIp(None, vm_ip, True)
    if not vm:
        print("Could not find a virtual machine with IP: ", vm_ip)
        exit(1)
    else:
        print("Found VM", )
    # vm_details(vm)
    print("Now adding Disk:")
    add_disk(vm=vm, disk_size="8", disk_type='thin')
    vm_details.vm_details(vm)
    connection.disconnect_vc(si)


if __name__ == '__main__':
    main()
