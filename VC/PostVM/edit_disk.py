from VC import connection, vm_details, edit_vm
from pyVmomi import vim


def edit_disk(vm, disk_size, disk_type):
    spec = vim.vm.ConfigSpec()
    virtual_disk_device = None
    print("LL",vm.config)
    for dev in vm.config.hardware.device:
        if isinstance(dev, vim.vm.device.VirtualDisk):
            virtual_disk_device = dev
    if not virtual_disk_device:
        print("No Disk found")
        return False
    dev_changes = []
    new_disk_kb = int(disk_size) * 1024 * 1024
    disk_spec = vim.vm.device.VirtualDeviceSpec()
    # disk_spec.fileOperation = "create"
    disk_spec.operation = vim.vm.device.VirtualDeviceSpec.Operation.edit
    disk_spec.device = virtual_disk_device
    print("Current disk size: ", disk_spec.device.capacityInKB)
    if disk_spec.device.capacityInKB >= new_disk_kb:
        print("New disk is smaller then previous size")
        exit(0)
    disk_spec.device.capacityInKB = new_disk_kb
    dev_changes.append(disk_spec)

    spec.deviceChange = dev_changes
    edit_vm.edit_vm(vm, spec)


def main():
    si = connection.connect_vc(server='10.0.88.202', username='clouduser@vcn.local', password='gsLab!1231')
    if connection is not None:
        print("Session vm: ", si.content.sessionManager.currentSession.key)
    else:
        print("Not Connected to vc!")
        exit(0)
    vm_ip = "10.136.58.157"
    vm = None
    search_index = si.content.searchIndex
    vm = search_index.FindByIp(None, vm_ip, True)
    if not vm:
        print("Could not find a virtual machine with IP: ", vm_ip)
        exit(1)
    else:
        print("Found VM",)
    # vm_details(vm)
    print("Now adding Disk:")
    edit_disk(vm=vm, disk_size="42", disk_type='thin')
    vm_details.vm_details(vm)
    connection.disconnect_vc(si)


if __name__ == '__main__':
    main()
