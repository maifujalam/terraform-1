from VC import connection, vm_details, edit_vm
from pyVmomi import vim


def remove_disk(vm, disk_number):
    spec = vim.vm.ConfigSpec()
    virtual_disk_device = None
    devices = []
    if disk_number == 1:
        print("Cannot delete OS Disk.")
        exit(0)
    label = 'Hard disk '+disk_number
    for dev in vm.config.hardware.device:
        if isinstance(dev, vim.vm.device.VirtualDisk) and dev.deviceInfo.label == label:
            virtual_disk_device = dev
    if not virtual_disk_device:
        print("No Disk found to delete.")
        return False
    disk_spec = vim.vm.device.VirtualDeviceSpec()
    disk_spec.operation = vim.vm.device.VirtualDeviceSpec.Operation.remove
    disk_spec.device = virtual_disk_device
    devices.append(disk_spec)
    spec.deviceChange = devices
    ack = edit_vm.edit_vm(vm_obj=vm, spec=spec)
    print("Ack: ", ack)


def main():
    si = connection.connect_vc(server='10.0.88.202', username='clouduser@vcn.local', password='gsLab!1231')
    if connection is not None:
        print("Session vm: ", si.content.sessionManager.currentSession.key)
    else:
        print("Not Connected to vc!")
        exit(0)
    vm_ip = "10.136.58.157"
    vm = None
    search_index = si.content.searchIndex
    vm = search_index.FindByIp(None, vm_ip, True)
    if not vm:
        print("Could not find a virtual machine with IP: ", vm_ip)
        exit(1)
    else:
        print("Found VM", )
    # vm_details(vm)
    print("Now Removing Disk:")
    remove_disk(vm=vm, disk_number='2')
    vm_details.vm_details(vm)
    connection.disconnect_vc(si)


if __name__ == '__main__':
    main()
