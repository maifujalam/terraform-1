from pyVim import connect

"""
This module is used to connect and disconnect to VCenter.
"""


def connect_vc(server, username, password):
    """
    Todo: Connect to VC
    :param server: Server ip
    :param username: Username
    :param password: Password
    :return: Service Instance(si) or None(If connection error)
    """
    no_ssl = True
    si = None    # Service Instance
    try:
        if no_ssl:
            si = connect.SmartConnectNoSSL(
                host=server,
                user=username,
                pwd=password,
                port=443)
            print("in SmartConnectNoSSL")
        else:
            si = connect.SmartConnect(
                host=server,
                user=username,
                pwd=password,
                port=443)
        content = si.RetrieveContent()
        if content:
            return si
        return None
    except RecursionError:
        return False
    except Exception:
        return False


def disconnect_vc(si):
    """
    Todo: Disconnect to VC
    :param si: Service Instance
    """
    si.content.sessionManager.Logout()
