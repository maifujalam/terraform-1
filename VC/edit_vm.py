from pyVim import connect, task


def edit_vm(vm_obj=None, spec=None):
    """
    Todo: Edit to VM instance based on provided specification
    :param vm_obj:  VM Object
    :param spec:  Specification List
    :return: Task Completion vm or None(if task fails)
    """
    print("Executing edit vm.")
    try:
        reconfig_task = vm_obj.ReconfigVM_Task(spec)
        task.WaitForTask(reconfig_task)
        return reconfig_task.info.key
    except Exception as e:
        print("Exception while editing virtual machine in vcenter : ")
    return None
