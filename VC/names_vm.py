from VC import connection

MAX_DEPTH = 10


def printvminfo(vm, depth=1):
    """
    Print information for a particular virtual machine or recurse into a folder
    with depth protection
    """

    # if this is a group it will have children. if it does, recurse into them
    # and then return
    if hasattr(vm, 'childEntity'):
        if depth > MAX_DEPTH:
            return
        vmlist = vm.childEntity
        for child in vmlist:
            printvminfo(child, depth + 1)
        return

    summary = vm.summary
    print(summary.config.name)


def main():
    si = connection.connect_vc(server='10.0.88.202', username='clouduser@vcn.local', password='gsLab!1231')
    if connection is not None:
        print("Session vm: ", si.content.sessionManager.currentSession.key)
    else:
        print("Not Connected to vc!")
        exit(0)
    content = si.RetrieveContent()
    for child in content.rootFolder.childEntity:
        if hasattr(child, 'vmFolder'):
            datacenter = child
            vmfolder = datacenter.vmFolder
            vmlist = vmfolder.childEntity
            for vm in vmlist:
                printvminfo(vm)
    connection.disconnect_vc(si)


if __name__ == '__main__':
    main()
