from VC import connection
from pyVmomi import vim
import re
from VC import vm_details

"""
This module search VMs on basis of vm_name,vm_ip,vm_instance_uuid
"""

vm_name = ""
vm_ip = ""
vm_instance_uuid = "503fade8-f7c0-5b93-dabb-462d497f1e5c"


def search_vm_by_name(service_instance, name, strict=False):
    """
    Todo: Search virtual machine by name
    :param service_instance: Service Instance
    :param name: Name of the VM to Search
    :param strict: Strict Search
    :returns: A virtual machine object
    """
    content = service_instance.content
    root_folder = content.rootFolder
    objView = content.viewManager.CreateContainerView(root_folder,
                                                      [vim.VirtualMachine],
                                                      True)
    vmList = objView.view
    objView.Destroy()
    obj = []
    for vm in vmList:
        if strict:
            if vm.name == name:
                obj.append(vm)
                return obj
        else:
            if re.match(".*{}.*".format(name), vm.name):
                obj.append(vm)
    return obj


def search_vm_by_ip(service_instance, ip):
    """
    Todo: Search VM on the basis of IP Address of VM.Please note,it does not work if vm is off.
    :param service_instance:
    :param ip:
    :return obj: VM object or None if not found
    """
    search_index = service_instance.content.searchIndex
    vm = None
    vm = search_index.FindByIp(None, ip, True)
    return vm


def search_vm_by_uuid(service_instance, instance_uuid):
    """
    Todo: Search VM on the basis of instance_uuid.
    :param service_instance:
    :param  instance_uuid:
    :return obj: VM object or None if not found
    """
    search_index = service_instance.content.searchIndex
    vm = None
    vm = search_index.FindByUuid(None, vm_instance_uuid, True, True)
    return vm


def main():
    si = connection.connect_vc(server='10.0.88.202', username='clouduser@vcn.local', password='gsLab!1231')
    if connection is not None:
        print("Session vm: ", si.content.sessionManager.currentSession.key)
    else:
        print("Not Connected to vc!")
        exit(0)
    vms = []
    if vm_name != "":
        print("Finding VM with name: ", vm_name)
        vms = search_vm_by_name(service_instance=si, name=vm_name, strict=False)
    elif vm_ip != "" or vm_instance_uuid:
        if vm_ip != "":
            print("Finding VM with IP: ", vm_ip)
            search_index = si.content.searchIndex
            vm = search_index.FindByIp(None, vm_ip, True)
            vms.append(vm)
        if vm_instance_uuid != "":
            print("Finding VM with UUID: ", vm_instance_uuid)
            search_index = si.content.searchIndex
            vm = search_index.FindByUuid(None, vm_instance_uuid, True, True)
            vms.append(vm)
    else:
        print("Please provide any of vm_name,vm_ip,vm_uuid")
    if len(vms) == 0:
        print("No vms found with given details.")
    else:
        print("Total VM Found: ", len(vms))
    for vm in vms:
        vm_details.vm_details(vm)
    connection.disconnect_vc(si)


if __name__ == '__main__':
    main()
