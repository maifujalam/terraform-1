from VC import connection
from pyVmomi import vim

"""
It prints the details of a VM on the basis of VM Object.
"""


def vm_details(vm):
    """
    Todo: Print the details of a VM based on vm object.
    :param vm: VM Object
    """
    config_spec = vim.vm.ConfigSpec()
    print("Found Virtual Machine")
    print("=====================")
    details = {'name': vm.summary.config.name,
               'instance UUID': vm.summary.config.instanceUuid,
               'bios UUID': vm.summary.config.uuid,
               'path to VM': vm.summary.config.vmPathName,
               'guest OS id': vm.summary.config.guestId,
               'guest OS name': vm.summary.config.guestFullName,
               'host name': vm.runtime.host.name,
               'last booted timestamp': vm.runtime.bootTime,
               }

    for name, value in details.items():
        print(u"  {0:{width}{base}}: {1}".format(name, value, width=25, base='s'))

    print(u"  Devices:")
    print(u"  --------")
    for device in vm.config.hardware.device:
        # diving into each device, we pull out a few interesting bits
        dev_details = {'vm': device.key,
                       'summary': device.deviceInfo.summary,
                       'device type': type(device).__name__,
                       'backing type': type(device.backing).__name__}

        print(u"  label: {0}".format(device.deviceInfo.label))
        print(u"  ------------------")
        for name, value in dev_details.items():
            print(u"    {0:{width}{base}}: {1}".format(name, value,
                                                       width=15, base='s'))

        if device.backing is None:
            continue

        # the following is a bit of a hack, but it lets us build a summary
        # without making many assumptions about the backing type, if the
        # backing type has a file name we *know* it's sitting on a datastore
        # and will have to have all of the following attributes.
        if hasattr(device.backing, 'fileName'):
            datastore = device.backing.datastore
            if datastore:
                print(u"    datastore")
                print(u"        name: {0}".format(datastore.name))
                # there may be multiple hosts, the host property
                # is a host mount info type not a host system type
                # but we can navigate to the host system from there
                for host_mount in datastore.host:
                    host_system = host_mount.key
                    print(u"        host: {0}".format(host_system.name))
                print(u"        summary")
                summary = {'capacity': datastore.summary.capacity,
                           'freeSpace': datastore.summary.freeSpace,
                           'file system': datastore.summary.type,
                           'url': datastore.summary.url}
                for key, val in summary.items():
                    print(u"            {0}: {1}".format(key, val))
            print(u"    fileName: {0}".format(device.backing.fileName))
            print(u"    device ID: {0}".format(device.backing.backingObjectId))

        print(u"  ------------------")


if __name__ == '__main__':
    si = connection.connect_vc(server='10.0.88.202', username='clouduser@vcn.local', password='gsLab!1231')
    if connection is not None:
        print("Session vm: ", si.content.sessionManager.currentSession.key)
    else:
        print("Not Connected to vc!")
        exit(0)
    search_index = si.content.searchIndex
    vm_ip = "10.136.58.157"
    vm = search_index.FindByIp(None, vm_ip, True)
    vm_details(vm)
